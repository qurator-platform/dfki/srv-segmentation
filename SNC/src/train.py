import json
import torch
import logging
import os
from transformers.file_utils import is_tf_available, is_torch_available
from transformers import BertTokenizerFast, BertForSequenceClassification,BertModel
from transformers import Trainer, TrainingArguments
import numpy as np
import random
import shutil
import argparse
from sklearn.metrics import accuracy_score
import torch.nn as nn
from modeling.neural import MultiHeadedAttention, PositionwiseFeedForward
from torch.nn import BCEWithLogitsLoss, CrossEntropyLoss, MSELoss
from transformers.models.bert.modeling_bert import BertPreTrainedModel,SequenceClassifierOutput,BertPooler
import glob


BASE_LM_DIC={'bert-base':'bert-base-uncased',
             'pubmedbert-base':'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext'}

SEC_CLS=['introduction', 'background', 'case', 'method', 'result', 'discussion', 'conclusion', 'additional']


def compute_metrics(pred):
  labels = pred.label_ids
 
  preds = pred.predictions.argmax(-1)
  
  # calculate accuracy using sklearn's function
  acc = accuracy_score(labels, preds)
  return {
      'accuracy': acc,
  }

def set_seed(seed: int):
    """
    Helper function for reproducible behavior to set the seed in ``random``, ``numpy``, ``torch`` and/or ``tf`` (if
    installed).

    Args:
        seed (:obj:`int`): The seed to set.
    """
    random.seed(seed)
    np.random.seed(seed)
    if is_torch_available():
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        # ^^ safe to call this function even if cuda is not available
    if is_tf_available():
        import tensorflow as tf

        tf.random.set_seed(seed)
        
def init_logger(log_file=None, log_file_level=logging.NOTSET):
    log_format = logging.Formatter("[%(asctime)s %(levelname)s] %(message)s")
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_format)
    logger.handlers = [console_handler]

    if log_file and log_file != '':
        file_handler = logging.FileHandler(log_file)
        file_handler.setLevel(log_file_level)
        file_handler.setFormatter(log_format)
        logger.addHandler(file_handler)

    return logger


class PubmedSecClsDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {k: torch.tensor(v[idx]) for k, v in self.encodings.items()}
        item["labels"] = torch.tensor([self.labels[idx]])
        return item

    def __len__(self):
        return len(self.labels)
    
def load_data(path):
    data=torch.load(path)
    texts = [d['section_text'] for d in data]
    labels = [d['label_id'] for d in data]
    return texts, labels

class TransformerEncoderLayer(nn.Module):
    def __init__(self, d_model, heads, d_ff, dropout):
        super(TransformerEncoderLayer, self).__init__()

        self.self_attn = MultiHeadedAttention(
            heads, d_model, dropout=dropout)
        self.feed_forward = PositionwiseFeedForward(d_model, d_ff, dropout)
        self.layer_norm = nn.LayerNorm(d_model, eps=1e-6)
        self.dropout = nn.Dropout(dropout)

    def forward(self, iter, query, inputs):#, mask):
        if (iter != 0):
            input_norm = self.layer_norm(inputs)
        else:
            input_norm = inputs

        
        context = self.self_attn(input_norm, input_norm, input_norm)#,mask=mask)
        out = self.dropout(context) + inputs
        return self.feed_forward(out)
    


class CLSTransformerEncoder(nn.Module):
    def __init__(self, model, args, d_model, d_ff, heads, dropout, num_inter_layers=0):
        super(CLSTransformerEncoder, self).__init__()
        self.args = args
        self.d_model = d_model
        self.num_inter_layers = num_inter_layers
        
        init_logger(args.log_file)
 
        self.transformer_inter = nn.ModuleList(
            [TransformerEncoderLayer(d_model, heads, d_ff, dropout)
             for _ in range(num_inter_layers)])
        self.dropout = nn.Dropout(dropout)
        self.layer_norm = nn.LayerNorm(d_model, eps=1e-6)
        self.pooler = BertPooler(model.config)
        self.wo = nn.Linear(d_model, args.num_labels)
        self.sigmoid = nn.Sigmoid()
        #self.softmax = nn.Softmax(dim=1)
        
        

    def forward(self, sec_emb):
        """ See :obj:`EncoderBase.forward()`"""

        for i in range(self.num_inter_layers):
            x = self.transformer_inter[i](i, sec_emb, sec_emb)

        x = self.layer_norm(x)
        x = self.pooler(x)
        x = self.wo(x)
        re = self.sigmoid(x)
        return re
    

   
class BertForSecClassification(BertPreTrainedModel):
    def __init__(self, config):
        super().__init__(config)
        self.num_labels = config.num_labels
        self.config = config

        self.bert = BertModel(config)
        self.dropout = nn.Dropout(config.hidden_dropout_prob)
       
        self.classifier = nn.Linear(config.hidden_size, config.num_labels)

        self.init_weights()


    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        labels=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
    ):
        r"""
        labels (:obj:`torch.LongTensor` of shape :obj:`(batch_size,)`, `optional`):
            Labels for computing the sequence classification/regression loss. Indices should be in :obj:`[0, ...,
            config.num_labels - 1]`. If :obj:`config.num_labels == 1` a regression loss is computed (Mean-Square loss),
            If :obj:`config.num_labels > 1` a classification loss is computed (Cross-Entropy).
        """
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )

        output = outputs[0]

        output = self.dropout(output)
        logits = self.classifier(output)

        loss = None
        if labels is not None:
            if self.config.problem_type is None:
                if self.num_labels == 1:
                    self.config.problem_type = "regression"
                elif self.num_labels > 1 and (labels.dtype == torch.long or labels.dtype == torch.int):
                    self.config.problem_type = "single_label_classification"
                else:
                    self.config.problem_type = "multi_label_classification"

            if self.config.problem_type == "regression":
                loss_fct = MSELoss()
                if self.num_labels == 1:
                    loss = loss_fct(logits.squeeze(), labels.squeeze())
                else:
                    loss = loss_fct(logits, labels)
            elif self.config.problem_type == "single_label_classification":
                loss_fct = CrossEntropyLoss()
                loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))
            elif self.config.problem_type == "multi_label_classification":
                loss_fct = BCEWithLogitsLoss()
                loss = loss_fct(logits, labels)
        if not return_dict:
            output = (logits,) + outputs[2:]
            return ((loss,) + output) if loss is not None else output

        return SequenceClassifierOutput(
            loss=loss,
            logits=logits,
            hidden_states=outputs.hidden_states,
            attentions=outputs.attentions,
        )
        
def train(args):
    
    # set seed
    set_seed(args.seed)
    
    #data
    TRAIN_PATH = args.data_path+'/pubmed_sec_cls_TRAIN.pt'
    TEST_PATH = args.data_path+'/pubmed_sec_cls_TEST.pt'
    
    #create save folder if not exisits
    print('#'*80)
    print('Model saving folder: '+args.model_path)
    print('#'*80)
    if not os.path.exists(args.model_path):
        os.mkdir(args.model_path)
        print('Model folder created.')
    else:
        if len(os.listdir(args.model_path))!= 0:
            text = input('Model folder already exisits and is not empty. Do you want to remove it and redo preprocessing (yes or no) ?')
            if text.lower()=='yes':
                shutil.rmtree(args.model_path)
                os.mkdir(args.model_path)
                print('YES: Model folder removed and recreated.')
            else:
                print('NO: Program stopped.')
                exit()
    
    # init logger
    init_logger(args.log_file)
    logger = logging.getLogger()
    
    # obtain class names
    with open(args.sn_dic_path, encoding='utf-8') as file:
        dic = json.load(file)
    SEC_CLS = list(dic.keys())
    assert args.num_labels==len(SEC_CLS)
    
    logger.info('%i section classes : %s'%(args.num_labels,str(SEC_CLS)))
    
    # load data
    logger.info('Loading training data from: %s'%(TRAIN_PATH))
    train_texts, train_labels = load_data(TRAIN_PATH)
    logger.info('Loading validation data from: %s'%(TEST_PATH))
    valid_texts, valid_labels = load_data(TEST_PATH)
    
    logger.info('there are %i training samples'%(len(train_labels)))
    logger.info('there are %i validation samples'%(len(valid_labels)))
    
    # load the tokenizer
    logger.info('Loading tokenizer: %s from %s'%(args.base_LM,BASE_LM_DIC[args.base_LM]))
    tokenizer = BertTokenizerFast.from_pretrained(BASE_LM_DIC[args.base_LM], do_lower_case=True, cache_dir=args.temp_dir)
    
    # tokenize the dataset, truncate when passed `max_length`, 
    # and pad with 0's when less than `max_length`
    logger.info('Tokenizing training data ...')
    train_encodings = tokenizer(train_texts, truncation=True, padding=True, max_length=args.max_len)
    logger.info('Tokenizing validation data ...')
    valid_encodings = tokenizer(valid_texts, truncation=True, padding=True, max_length=args.max_len)
    

    
    # convert our tokenized data into a torch Dataset
    train_dataset = PubmedSecClsDataset(train_encodings, train_labels)
    valid_dataset = PubmedSecClsDataset(valid_encodings, valid_labels)
    logger.info('Data perpared.')
    
    # load the model and pass to CUDA
    logger.info('Loading model: %s from %s'%(args.base_LM, BASE_LM_DIC[args.base_LM]))
    
    
    if args.classifier=='transformer':
        model = BertForSecClassification.from_pretrained(BASE_LM_DIC[args.base_LM], num_labels=args.num_labels,cache_dir=args.temp_dir).to("cuda")
        model.classifier = CLSTransformerEncoder(model, args, model.config.hidden_size, args.ext_ff_size, args.ext_heads,
                                               args.ext_dropout, args.ext_layers)
    else:
        model = BertForSequenceClassification.from_pretrained(BASE_LM_DIC[args.base_LM], num_labels=args.num_labels,cache_dir=args.temp_dir).to("cuda")
        
        
    logger.info('Classifier: %s'%(args.classifier.upper()))
    logger.info(model)
    
    
    # training arguments
    training_args = TrainingArguments(
                    output_dir=args.model_path,      # output directory
                    num_train_epochs=args.epochs,              # total number of training epochs
                    per_device_train_batch_size=args.train_batch, # batch size per device during training
                    per_device_eval_batch_size=args.eval_batch,  # batch size for evaluation
                    warmup_steps=args.warmup_steps,                # number of warmup steps for learning rate scheduler
                    weight_decay=args.weight_decay,               # strength of weight decay
                    logging_dir=args.model_path+'/logs',            # directory for storing logs
                    load_best_model_at_end=True,     # load the best model when finished training (default metric is loss)
                    # but you can specify `metric_for_best_model` argument to change to accuracy or other metric
                    logging_steps=args.logging_steps,               # log & save weights each logging_steps
                    evaluation_strategy="steps",     # evaluate each `logging_steps`
                )
    
    logger.info(str(training_args))
    
    # trainer
    trainer = Trainer(
                    model=model,                         # the instantiated Transformers model to be trained
                    args=training_args,                  # training arguments, defined above
                    train_dataset=train_dataset,         # training dataset
                    eval_dataset=valid_dataset,          # evaluation dataset
                    compute_metrics=compute_metrics,     # the callback that computes metrics of interest
                )
    
    # train the model
    logger.info('Start training...')
    trainer.train()
    logger.info('Training DONE.')
    
def test_text_file(file,model,tokenizer,logger,args):
    logger.info('Start classifying texts in %s'%(file))
    result_file = file.split('.txt')[0]+'.cls.result.json'
    with open(file) as f:
        texts=json.load(f)
        
    cls_results=[]
    for i,text in enumerate(texts):
        inputs = tokenizer(text, truncation=True, padding=True, max_length=args.max_len,return_tensors="pt")
        outputs = model(**inputs)
        logits = outputs.logits#.softmax(1)
        label_id = logits.argmax().item()
        label = SEC_CLS[label_id]
        logger.info('TEXT_%i: %i --- %s'%(i,label_id,label))
        
        cls_results.append((label_id,label))
    logger.info('Save classification results to %s'%(result_file))
    with open(result_file,'w+') as f:
        json.dump(cls_results,f)
        
    return cls_results
    
    
def test_text(args):
   
    # init logger
    init_logger(args.log_file)
    logger = logging.getLogger()
    
    #load model
    logger.info('Loading model from %s' % args.test_from)
    
    model=BertForSequenceClassification.from_pretrained(args.test_from+'/', num_labels=args.num_labels,cache_dir=args.temp_dir)
    model.eval()
    tokenizer = BertTokenizerFast.from_pretrained('bert-base-uncased', do_lower_case=True, cache_dir=args.temp_dir)
    
    
    text_files = sorted(glob.glob(os.path.join(args.text_path, '*.txt')))
    results=[]
    for file in text_files:
        re=test_text_file(file,model,tokenizer,logger,args)
        results.append(re)
    return results
        

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-mode", default='train', type=str)
    parser.add_argument("-data_path", default='data_pubmed/data_pubmed_sec_cls', type=str)
    parser.add_argument("-model_path", default='', type=str)
    parser.add_argument("-base_LM", default='bert-base-uncased', type=str)
    parser.add_argument("-max_len", default=512, type=int)
    parser.add_argument("-epochs", default=3, type=int)
    parser.add_argument("-train_batch", default=16, type=int)
    parser.add_argument("-eval_batch", default=100, type=int)
    parser.add_argument("-logging_steps", default=1000, type=int)
    parser.add_argument("-warmup_steps", default=500, type=int)
    parser.add_argument("-weight_decay", default=0.01, type=int)
    parser.add_argument("-seed", default=1, type=int)
    parser.add_argument("-temp_dir", default='temp', type=str)
    parser.add_argument("-sn_dic_path", default='data_pubmed/data_pubmed_raw/SN_dic_8_Added.json', type=str)
    parser.add_argument("-num_labels", default=8, type=int)
    parser.add_argument("-classifier", default='linear', type=str)
    parser.add_argument("-log_file", default='', type=str)
    
    #transformer classifier
    parser.add_argument("-ext_dropout", default=0.2, type=float)
    parser.add_argument("-ext_layers", default=2, type=int)
    parser.add_argument("-ext_hidden_size", default=768, type=int)
    parser.add_argument("-ext_heads", default=8, type=int)
    parser.add_argument("-ext_ff_size", default=2048, type=int)
    
    #test_text
    parser.add_argument("-test_from", default='', type=str)
    parser.add_argument("-text_path", default='', type=str)
    
    
    args = parser.parse_args()
    
    if args.classifier=='transformer':
        SAVE_MODEL_NAME = 'CLStrans_'+args.base_LM.split('-')[0]+args.base_LM.split('-')[1][0].upper()+'_ep'+str(args.epochs)+'tb'+str(args.train_batch)+'eb'+str(args.eval_batch)+'ws'+str(args.warmup_steps)+'ls'+str(args.logging_steps)+'wd'+str(args.weight_decay)
    else:    
        SAVE_MODEL_NAME = 'CLS_'+args.base_LM.split('-')[0]+args.base_LM.split('-')[1][0].upper()+'_ep'+str(args.epochs)+'tb'+str(args.train_batch)+'eb'+str(args.eval_batch)+'ws'+str(args.warmup_steps)+'ls'+str(args.logging_steps)+'wd'+str(args.weight_decay)
    if args.seed!=1:
        SAVE_MODEL_NAME  = SAVE_MODEL_NAME +'_seed'+str(args.seed)
        
    SAVE_MODEL_PATH = 'models/'+SAVE_MODEL_NAME     
    
    if args.model_path=='':
        args.model_path=SAVE_MODEL_PATH
    
    
    if (args.mode == 'train'):
        if args.log_file=='':
            args.log_file = args.model_path+'/train.log'
        train(args)
        os.mkdir(args.model_path+'/DONE')
    elif (args.mode == 'test_text'):
        if args.log_file=='':
            args.log_file = args.text_path+'/test_text.log'
        test_text(args)
        os.mkdir(args.text_path+'/DONE')
        


if __name__ == "__main__":
    main()
