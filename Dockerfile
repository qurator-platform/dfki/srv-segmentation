#FROM maven:3.6-jdk-11-slim
FROM openkbs/jdk11-mvn-py3:latest

MAINTAINER Julian Moreno Schneider <julian.moreno_schneider@dfki.de>

RUN sudo apt-get update 
RUN sudo apt-get install -y python3-pip 
RUN pip3 install --user fuzzywuzzy pandas lxml regex sklearn

RUN sudo mkdir /var/maven/ && sudo chmod -R 777 /var/maven 
RUN sudo mkdir /tmp/srv-segmentation/ && sudo chmod -R 777 /tmp/srv-segmentation
ENV MAVEN_CONFIG /var/maven/.m2

ADD SNC/requirements.txt /requirements.txt
RUN pip3 install --user -r /requirements.txt

ADD pom.xml /tmp/srv-segmentation
#COPY lib /tmp/srv-text-qa/lib
RUN cd /tmp/srv-segmentation && mvn -B -e -C -T 1C -Duser.home=/var/maven org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline 

RUN sudo chmod -R 777 /var/maven

ADD SNC/ /tmp/srv-segmentation/SNC/ 
ADD test_texts/ /tmp/srv-segmentation/test_texts/ 
ADD Titles/ /tmp/srv-segmentation/Titles/
ADD src/ /tmp/srv-segmentation/src/ 

#COPY src/main/resources/application_server.properties /tmp/srv-segmentation/src/main/resources/application.properties
WORKDIR /tmp/srv-segmentation
RUN mvn -Duser.home=/var/maven clean package -DskipTests

RUN sudo chmod -R 777 /tmp/srv-segmentation/test_texts/

#RUN sudo mkdir /tmp/tmpFiles/ && sudo chmod -R 777 /tmp/tmpFiles/

EXPOSE 8080

CMD mvn -Duser.home=/var/maven spring-boot:run -Dspring-boot.run.jvmArguments="-Xms2048m -Xmx4096m"
#CMD ["/bin/bash"]

###FROM adoptopenjdk/openjdk8-openj9:alpine
##FROM openkbs/jdk-mvn-py3
###
###RUN /usr/bin/python3 -m pip install --upgrade pip
##RUN mkdir /tmp/srv-segmentation && chmod -R 777 /tmp/srv-segmentation
##COPY --from=0 /requirements.txt  /tmp/srv-segmentation/requirements.txt
##RUN pip install --user -r /tmp/srv-segmentation/requirements.txt
##EXPOSE 8080
##COPY --from=0 /tmp/srv-segmentation/test_texts  /tmp/srv-segmentation/test_texts
##COPY --from=0 /tmp/srv-segmentation/Titles  /tmp/srv-segmentation/Titles
##COPY --from=0 /tmp/srv-segmentation/SNC  /tmp/srv-segmentation/SNC
##
##COPY --from=0 /tmp/srv-segmentation/target/datasegmentation-0.0.1-SNAPSHOT.jar  /tmp/srv-segmentation/datasegmentation-0.0.1-SNAPSHOT.jar
##
##WORKDIR /tmp/srv-segmentation
##
##RUN sudo chmod -R 777 *
###ENTRYPOINT ["java", "-jar", "/tmp/srv-segmentation/datasegmentation-0.0.1-SNAPSHOT.jar"]
##CMD ["/bin/bash"]
