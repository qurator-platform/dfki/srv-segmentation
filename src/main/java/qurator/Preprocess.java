package qurator;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Preprocess {

	public static void main(String[] args) throws Exception {
		
		String path = "/Users/julianmorenoschneider/Downloads/Ulrike Code/OpenMedia_de_20150729";
		String path2 = "/Users/julianmorenoschneider/Downloads/Ulrike Code/OpenMedia_de_20150729/JSON_output";
		File folder = new File(path);
		File filees[]  = folder.listFiles();
		for (File file : filees) {
			if(file.getName().contains(".xml")) {
				System.out.println(file.getName());
				String content = FileUtils.readFileToString(file,"utf-8");
	//			System.out.println(content);
				Document doc = Jsoup.parse(content);
				Elements divs = doc.select("OM_FIELD");
				JSONObject json = new JSONObject();
				for (Element field : divs) {
					System.out.print("\t"+field.attr("Name")+": ");
					System.out.println(field.text());
					json.put(field.attr("Name"), field.text());
				}
				FileUtils.writeStringToFile(new File(path2+"/"+file.getName().replace("xml", "json")), json.toString(1), "utf-8");
	//			System.exit(0);
			}
		}
	}
}
