package de.hainet.datasegmentation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.tools.PDFText2HTML;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import de.hainet.datasegmentation.tree.TNode;
import net.sourceforge.tess4j.OCRResult;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.ITessAPI.TessOcrEngineMode;
import net.sourceforge.tess4j.ITessAPI.TessPageIteratorLevel;
import net.sourceforge.tess4j.ITessAPI.TessPageSegMode;
import net.sourceforge.tess4j.ITesseract.RenderedFormat;

public class PDFExtraction {

	public static void main(String[] args) throws Exception {
		extractPDFs();
		
		System.exit(0);
		
//		extractTestHeadings();
		
//		copyBioPapers();
		
//		extractHeadings();		

		
		List<TNode> trees = processTitles();
		statistics(trees,1);
	}

	public static void addLevelStatistics(HashMap<String, Integer> hash, TNode tree, int untilLevel, int level) {
		for (TNode tNode : tree.getChilds()) {
			if(untilLevel==level) {
				String title = tNode.getValue();
				if(hash.containsKey(title)) {
					hash.put(title, hash.get(title)+1);
				}
				else {
					hash.put(title, 1);
				}		
			}
			else {
				addLevelStatistics(hash, tNode, untilLevel, level+1);
			}
		}
	}
	
	public static void statistics(List<TNode> trees, int untilLevel) {
		int level = 1;
		HashMap<String, Integer> hash = new HashMap<String, Integer>();		
		for (TNode tree : trees) {
			addLevelStatistics(hash,tree,untilLevel,level);
		}
		LinkedHashMap<String, Integer> orderedMap = new LinkedHashMap<String, Integer>();
		while(!hash.isEmpty()) {
			Integer v = 0;
			String k = null;
			for (String key : hash.keySet()) {
				if(hash.get(key)>v) {
					k = key;
					v=hash.get(key);
				}
//				if(hash.get(key)>0) {
//					System.out.println(key + "::: "+hash.get(key));
//				}
			}
			if(v>1) {
				orderedMap.put(k, v);
			}
			hash.remove(k);
		}

		for (String key : orderedMap.keySet()) {
			if(orderedMap.get(key)>0) {
				System.out.println(key + "::: "+orderedMap.get(key));
			}
		}
	}
	
	
	public static List<TNode> processTitles() throws Exception {
		List<TNode> trees = new LinkedList<TNode>();
//		HashMap<String, Integer> hash = new HashMap<String, Integer>();		
		String path = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/titles/";
		File folder = new File(path);
		File files[] = folder.listFiles();
		for (File file : files) {
			if(file.getName().contains(".txt")) {
				System.out.println(file.getName());
				List<String> titles = FileUtils.readLines(file,"utf-8");
				
				String typeOfValue = "None";
//				int currentValue = 0;
				String oldKey = null;
				boolean first = true;
				
				TNode tree = new TNode(new LinkedList<TNode>(), null, file.getName(), file.getName());
				for (String title : titles) {
//					title = title.substring(title.indexOf(" ")+1).toLowerCase();
					title = title.toLowerCase();
					if(first && title.indexOf("introduction")==-1) {
						continue;
					}
					if(first==true && (title.length()<6 || title.contains("{") || title.contains("et al") || title.contains(",")) ) {
//						System.out.println(first + "-->" + title);
						continue;
					}
					else if(title.length()<6 || title.contains("{") || title.contains("et al")) {
//						System.out.println(first +"==>" + title);
						continue;
					}
					String k = title.substring(0, title.indexOf(" "));
					String v = title.substring(title.indexOf(" "));					
//					String heading1Regex = "^([VI\\d]+)\\.([VI\\d]\\.)?\\s(?<title>.*?)$";
//					System.out.println(k+"--"+v);
					String type = null;
					if(k.matches("^([\\d]+)\\.([\\d]\\.)?$")) {
						type = "NP";
						if(typeOfValue.equalsIgnoreCase("None")) {
							typeOfValue = "NP";	
						}
//						System.out.println("Numerical: "+k);
					}
					else if(k.matches("^([\\d]+)\\.?([\\d]\\.?)?$")) {
						type = "N";
						if(typeOfValue.equalsIgnoreCase("None")) {
							typeOfValue = "N";	
						}
//						System.out.println("Numerical: "+k);
					}
					else if(k.matches("^([viVI]+)\\.([viVI]\\.)?$")) {
						type = "RP";
						if(typeOfValue.equalsIgnoreCase("None")) {
							typeOfValue = "RP";
						}
//						System.out.println("Roman: "+k);
					}
					else if(k.matches("^([viVI]+)\\.?([viVI]\\.?)?$")) {
						type = "R";
						if(typeOfValue.equalsIgnoreCase("None")) {
							typeOfValue = "R";
						}
//						System.out.println("Roman: "+k);
					}
					else {
						type = "O";
						if(typeOfValue.equalsIgnoreCase("None")) {
							typeOfValue = "O";
						}
//						System.out.println("Other");
					}
					if(!type.equalsIgnoreCase(typeOfValue)) {
						continue;
					}
//					System.out.println("Same type");
					if(first) {
						oldKey = k;
						TNode node = new TNode(new LinkedList<TNode>(), null, k, v);
						tree.addTNode(node);
						first = false;
					}
					else if (nextValue(oldKey, k, typeOfValue)) {
						oldKey = k;
						TNode node = new TNode(new LinkedList<TNode>(), null, k, v);
						tree.addTNode(node);
						first = false;
					}
					else if (isChild(oldKey, k, typeOfValue)) {
//						System.out.println("isChild");
//						oldKey = k;
						TNode node = new TNode(new LinkedList<TNode>(), null, k, v);
//						tree.addTNode(node);
						tree.addChildToTNode(node,oldKey);
						first = false;
					}
					else {
					}
				}
//				if(tree.hasChilds()) {
//					tree.printNode(" ", " ");
////					System.exit(0);
//				}
//				else {
//					System.out.println("No titles.");
//				}
//				System.out.println("----------------------------");
//				System.out.println("----------------------------");
//				System.out.println("----------------------------");
//				System.out.println("----------------------------");
				trees.add(tree);
			}
			
		}
		return trees;
	}

	public static boolean nextValue(String oldKey, String newKey, String type) {
		if(type.equalsIgnoreCase("N") || type.equalsIgnoreCase("NP")) {
			int ok = (oldKey.contains("."))?Integer.parseInt(oldKey.substring(0, oldKey.indexOf('.'))):Integer.parseInt(oldKey);
			int nk = (newKey.contains("."))?Integer.parseInt(newKey.substring(0, newKey.indexOf('.'))):Integer.parseInt(newKey);
			if(nk==ok+1) {
				return true;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("R") || type.equalsIgnoreCase("RP")) {
			oldKey = oldKey.substring(0, oldKey.indexOf('.'));
			newKey = newKey.substring(0, newKey.indexOf('.'));
			if(oldKey.equalsIgnoreCase("i")) {
				return (newKey.equalsIgnoreCase("ii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("ii")) {
				return (newKey.equalsIgnoreCase("iii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("iii")) {
				return (newKey.equalsIgnoreCase("iv"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("iv")) {
				return (newKey.equalsIgnoreCase("v"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("v")) {
				return (newKey.equalsIgnoreCase("vi"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("vi")) {
				return (newKey.equalsIgnoreCase("vii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("vii")) {
				return (newKey.equalsIgnoreCase("viii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("viii")) {
				return (newKey.equalsIgnoreCase("ix"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("ix")) {
				return (newKey.equalsIgnoreCase("x"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("x")) {
				return (newKey.equalsIgnoreCase("xi"))?true:false;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("O")) {
			return false;
		}
		else {
			System.out.println("ERROR: Type not supported.");
			return false;
		}
	}
	
	public static boolean isChild(String oldKey, String newKey, String type) {
		if(oldKey.equalsIgnoreCase(newKey)) {
			return false;
		}
		if(type.equalsIgnoreCase("N") || type.equalsIgnoreCase("NP")) {
			if(newKey.startsWith(oldKey)) {
				return true;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("R") || type.equalsIgnoreCase("RP")) {
			if(newKey.startsWith(oldKey)) {
				return true;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("O")) {
			return false;
		}
		else if(type.equalsIgnoreCase("M")) {
			return false;
		}
		else {
			System.out.println("ERROR: Type not supported.");
			return false;
		}
	}

	public static void classifyTitles() throws Exception {
		HashMap<String, Integer> hash = new HashMap<String, Integer>();		
		String path = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/titles/";
		File folder = new File(path);
		File files[] = folder.listFiles();
		for (File file : files) {
			if(file.getName().contains(".txt")) {
//				System.out.println(file.getName());
				List<String> titles = FileUtils.readLines(file,"utf-8");
				for (String title : titles) {
//					title = title.substring(title.indexOf(" ")+1).toLowerCase();
					title = title.toLowerCase();
					if(title.length()<6 || title.contains("{") || title.contains("et al") || title.contains(",")) {
						continue;
					}
					if(hash.containsKey(title)) {
						hash.put(title, hash.get(title)+1);
					}
					else {
						hash.put(title, 1);
					}
				}
			}
		}
		for (String key : hash.keySet()) {
			if(hash.get(key)>0) {
				System.out.println(key + "::: "+hash.get(key));
			}
		}
	}

	public static void extractTestHeadings() throws Exception {
	String content = FileUtils.readFileToString(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/555.html"),"utf-8");
//	String content = FileUtils.readFileToString(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/95066.html"),"utf-8");
//	System.out.println(content);
	Document doc = Jsoup.parse(content);
	
			List<String> titles = new LinkedList<String>();
			Elements divs = doc.select("div");
			for (Element div : divs) {
				if(div.hasAttr("style")) {
//					System.out.println("---------------------");
//					System.out.println("---------------------");
					Elements ps = div.getElementsByTag("p");
					for (Element p : ps) {
						//System.out.println(p);
						String text = p.text();
						String heading1Regex = "^([VI\\d]+)\\.([VI\\d]\\.)?\\s(?<title>.*?)$";

						if(text.matches(heading1Regex)) {
							if(text.indexOf(".", text.indexOf(" ")+1)!=-1) {
								titles.add(text.substring(0,text.indexOf(".", text.indexOf(" ")+1)));
							}
							else {
								titles.add(text);
							}
						}
						
					}
				}
			}
			titles.forEach(System.out::println);
	
	//doc.select("div").forEach(System.out::println);
	}


	public static void extractHeadings() throws Exception {
////		String content = FileUtils.readFileToString(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/6303.html"),"utf-8");
//		String content = FileUtils.readFileToString(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/555.html"),"utf-8");
////		System.out.println(content);
//		Document doc = Jsoup.parse(content);
//		
		
		String path = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/";
		String path2 = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/titles/";
		File folder = new File(path);
		File files[] = folder.listFiles();
		for (File file : files) {
			if(file.getName().contains(".html")) {
				System.out.println(file.getName());
				List<String> titles = new LinkedList<String>();
				String content = FileUtils.readFileToString(file,"utf-8");
				Document doc = Jsoup.parse(content);
				Elements divs = doc.select("div");
				for (Element div : divs) {
					if(div.hasAttr("style")) {
//						System.out.println("---------------------");
//						System.out.println("---------------------");
						Elements ps = div.getElementsByTag("p");
						for (Element p : ps) {
							//System.out.println(p);
							String text = p.text();
//							String heading1Regex = "^(\\d+)\\.(\\d\\.)?\\s(?<title>.*?)$";
							String heading1Regex = "^([viVI\\d]+)\\.?([viVI\\d]\\.?)?\\s(?<title>.*?)$";

							if(text.matches(heading1Regex)) {
								if(text.indexOf(".", text.indexOf(" ")+1)!=-1) {
									titles.add(text.substring(0,text.indexOf(".", text.indexOf(" ")+1)));
								}
								else {
									titles.add(text);
								}
							}
							
						}
					}
				}
//				titles.forEach(System.out::println);
				FileUtils.writeLines(new File(path2 + file.getName().replace("html", "txt")), "utf-8", titles);
			}
		}
		
		//doc.select("div").forEach(System.out::println);

		/*
		 * 
		 * Elements links = doc.select("a");
Elements sections = doc.select("section");
Elements logo = doc.select(".spring-logo--container");
Elements pagination = doc.select("#pagination_control");
Elements divsDescendant = doc.select("header div");
Elements divsDirect = doc.select("header > div");
You can also use more explicit methods inspired by the browser DOM instead of the generic select:

Element pag = doc.getElementById("pagination_control");
Elements desktopOnly = doc.getElementsByClass("desktopOnly");
		 */
		
		/**
		 * Extracting
		 * 
		 * Element firstArticle = doc.select("article").first();
Element timeElement = firstArticle.select("time").first();
String dateTimeOfFirstArticle = timeElement.attr("datetime");
Element sectionDiv = firstArticle.select("section div").first();
String sectionDivText = sectionDiv.text();
String articleHtml = firstArticle.html();
String outerHtml = firstArticle.outerHtml();
		 */
	}

	public static void extractPDFs() throws Exception {

//	    PDDocument doc = PDDocument.load(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555.pdf"));
//	    PDFTextStripper pdfStripper = new PDFTextStripper();
//	    System.out.println(new PDFTextStripper().getText(doc));

//	    RandomAccessBufferedFileInputStream raFile = new RandomAccessBufferedFileInputStream(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555.pdf"));
//	    PDFParser parser = new PDFParser(raFile);
//	    parser.parse();
//	    try (COSDocument cosDoc = parser.getDocument()) {
//	    	PDFTextStripper pdfStripper = new PDFText2HTML();
//	    	PDDocument pdDoc = new PDDocument(cosDoc);
////	        pdfStripper.setStartPage(1);
////	        pdfStripper.setEndPage(5);
//	    	String parsedText = pdfStripper.getText(pdDoc);
//	    	System.out.println(parsedText);
//	    }

		Tesseract tesseract = new Tesseract();
		System.out.println("Tesseract object created.");
        try {
            tesseract.setDatapath("/Users/julianmorenoschneider/Downloads/tessdata-master/tessdata");
    		System.out.println("Data path established.");
            // the path of your tess data folder
            // inside the extracted file
    		tesseract.setHocr(true);
    		tesseract.setOcrEngineMode(TessOcrEngineMode.OEM_TESSERACT_ONLY);
//    		tesseract.setTessVariable("options", "[\"--layout\", \"region\"]");
//    		tesseract.getSegmentedRegions(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555test.pdf"), 
//    				TessPageIteratorLevel.RIL_BLOCK);
//    		OCRResult res = tesseract.createDocumentsWithResults("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555test.pdf", 
//    				"/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555test_2", 
//    				Arrays.asList(new RenderedFormat[] {RenderedFormat.HOCR}), 
//    				TessPageIteratorLevel.RIL_BLOCK);
    		
    		tesseract.setPageSegMode(TessPageSegMode.PSM_SPARSE_TEXT_OSD);
//    		tesseract.setTessVariable("tessedit_write_block_separators", "TRUE");
    		tesseract.setTessVariable("tessedit_create_boxfile", "TRUE");
    		
            String text = tesseract.doOCR(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555test.pdf"));
    		System.out.println("OCR done.");
            // path of your image file
            System.out.print(text);
//            FileUtils.writeStringToFile(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555_tess.xml"), text, "utf-8");
            
			Document doc = Jsoup.parse(text);
			
			String cleanedText = "";
			
			Elements divs = doc.getElementsByAttributeValueContaining("class", "ocr_page");
			for (Element div : divs) {
				cleanedText += "<page>\n";
				Elements areas = div.getElementsByAttributeValueContaining("class", "ocr_carea");
				for (Element area : areas) {
					cleanedText += "<area>\n";
					Elements ps = area.getElementsByAttributeValueContaining("class", "ocr_par");
					for (Element p : ps) {
						cleanedText += "<paragraph>\n";
						Elements lines = p.getElementsByAttributeValueContaining("class", "ocr_line");
						for (Element line : lines) {
							cleanedText += "<line>\n";
							Elements words = line.getElementsByAttributeValueContaining("class", "ocrx_word");
							String lineWords = "";
							for (Element word : words) {
								lineWords += " " + word.text();
							}
							cleanedText += lineWords.trim() + "\n";
							cleanedText += "</line>\n";
						}
						cleanedText += "</paragraph>\n";
					}
					cleanedText += "</area>\n";
				}
				cleanedText += "</page>\n";
			}

            System.out.println(cleanedText);
        }
        catch (TesseractException e) {
            e.printStackTrace();
        }		
//		String path = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/";
//		String path2 = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/";
//		File folder = new File(path);
//		File files[] = folder.listFiles();
//		for (File file : files) {
//			if(file.getName().contains(".pdf")) {
//				System.out.println(file.getName());
//				
//			    PDDocument doc = PDDocument.load(file);
//			    String parsedText = new PDFText2HTML().getText(doc);
//			    FileUtils.writeStringToFile(new File(path2 + file.getName().replace("pdf", "html")), parsedText, "utf-8");
//	//		    RandomAccessBufferedFileInputStream raFile = new RandomAccessBufferedFileInputStream(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555.pdf"));
//	//		    PDFParser parser = new PDFParser(raFile);
//	//		    parser.parse();
//	//		    try (COSDocument cosDoc = parser.getDocument()) {
//	//		    	PDFTextStripper pdfStripper = new PDFText2HTML();
//	//		    	PDDocument pdDoc = new PDDocument(cosDoc);
//	////		        pdfStripper.setStartPage(1);
//	////		        pdfStripper.setEndPage(5);
//	//		    	String parsedText = pdfStripper.getText(pdDoc);
//	//		    	System.out.println(parsedText);
//	//		    }
//			}
//		}

		
//		String path = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/";
//		String path2 = "/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/html_output/";
//		File folder = new File(path);
//		File files[] = folder.listFiles();
//		for (File file : files) {
//			if(file.getName().contains(".pdf")) {
//				System.out.println(file.getName());
//				
//			    PDDocument doc = PDDocument.load(file);
//			    String parsedText = new PDFText2HTML().getText(doc);
//			    FileUtils.writeStringToFile(new File(path2 + file.getName().replace("pdf", "html")), parsedText, "utf-8");
//	//		    RandomAccessBufferedFileInputStream raFile = new RandomAccessBufferedFileInputStream(new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555.pdf"));
//	//		    PDFParser parser = new PDFParser(raFile);
//	//		    parser.parse();
//	//		    try (COSDocument cosDoc = parser.getDocument()) {
//	//		    	PDFTextStripper pdfStripper = new PDFText2HTML();
//	//		    	PDDocument pdDoc = new PDDocument(cosDoc);
//	////		        pdfStripper.setStartPage(1);
//	////		        pdfStripper.setEndPage(5);
//	//		    	String parsedText = pdfStripper.getText(pdDoc);
//	//		    	System.out.println(parsedText);
//	//		    }
//			}
//		}
	}
	    
	public static void copyBioPapers() {
		
//		int count = 0;
		File f = new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/arxiv_pdfs_metadata.json");
		String source = null;
		try {
			source = FileUtils.readFileToString(f,"utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray json = new JSONArray(source);
		json.forEach(obj -> {
			JSONArray jsonArr = (JSONArray) obj;
			JSONObject j2 = jsonArr.getJSONObject(1).getJSONObject("pids");
			System.out.println(j2.toString(1));
			if(j2.has("PubMed ID") || j2.has("PubMed Central ID")) {
				System.out.println("yes");
			}
			else {
				System.out.println("no");
			}
//			System.out.println(jsonArr.toString(1));
		});
		System.exit(0);

//		for value in data:   
//				#print(value)
//				if 'PubMed ID' in value[1]['pids']:
//					count += 1
//					#print(json.dumps(value, indent = 4, sort_keys=True))
//					print(value[0])
//					shutil.copy2('./pdfs/'+value[0], './biomedical/'+value[0])
//				elif 'PubMed Central ID' in value[1]['pids']:
//					#print(json.dumps(value, indent = 4, sort_keys=True))
//					count += 1
//			print(count)

		
	}
}
