package de.hainet.datasegmentation.api;

import java.io.File;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.hainet.datasegmentation.Segmentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("segmentation")
@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
//,
//        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class}
//        )
@OpenAPIDefinition(/*servers = {@Server(url = "https://apis.lynx-project.eu/api/named-entity-recognition")},*/
info = @Info(
		title = "Segmentation Module/Service",
		version = "0.0.1",
		description = "",
		contact = @Contact(url = "QURATOR Team",
		name = "QURATOR DFKI Team",
		email = "qurator-project@dfki.de")))
public class RestAPIController {

	Logger logger = Logger.getLogger(RestAPIController.class);

	@Autowired
	Segmentation segmentator;

	@PostConstruct
	public void setup() {
		try {
		} catch (Exception e) {
			logger.error(e.getMessage());
			System.exit(0);
		}
	}
	
	@Operation(hidden=true)
	@RequestMapping(value = "/testURL", method = { RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> testURL(
			@RequestBody(required = false) String postBody) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		ResponseEntity<String> response = new ResponseEntity<String>("The restcontroller is working properly", responseHeaders, HttpStatus.OK);
		return response;
	}

	@Operation(
			summary = "",
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Message stating that the document has been correctly processed.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
					@ApiResponse(responseCode = "400",
					description = "Bad request. Body cannot be null.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)),
					@ApiResponse(responseCode = "500",
					description = "An error has ocurred in the server.",
					content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))
			})
	@CrossOrigin
//	@RequestMapping(value = "/segmentDocument",
//	method = {	RequestMethod.POST },
//	consumes = {MediaType.APPLICATION_OCTET_STREAM_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE},
//	produces = {MediaType.APPLICATION_JSON_VALUE}
//			)
	@RequestMapping(value = "/segmentDocument",method = {	RequestMethod.POST })
	public ResponseEntity<String> segmentDocument(
			HttpServletRequest request,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam Map<String, String> allParams,
			@RequestParam(value = "content",required = false) MultipartFile content,
			@RequestParam(value = "request",required = false) String jsonRequest,
//	        @RequestPart("request") @Valid JSONObject jsonRequest,
//	        @RequestPart("content") @Valid @NotNull @NotBlank MultipartFile content,
			@RequestBody(required = false) String postBody) throws Exception {
		
//		if (postBody == null) {
//			String msg = "Body of POST call cannot be empty or null.";
//			logger.error("Exception: "+msg);
//			HttpHeaders responseHeaders = new HttpHeaders();
//			responseHeaders.add("Content-Type", "text/plain");
//			ResponseEntity<String> response = new ResponseEntity<String>(
//					msg, 
//					responseHeaders, 
//					HttpStatus.BAD_REQUEST);
//			return response;
////			throw new Exception(msg);
//		}
		
		/**
		 * Content-Type: multipart/form-data with two parts, 
		 * the first part named "request" will be application/json conforming to the following structure, 
		 * and the second part named "content" will be audio/x-wav or audio/mpeg
		 */
//		JSONObject jsonBody = new JSONObject(postBody);
//		String text = jsonBody.getString("text");
//		HashMap<String,String> metadata = new ObjectMapper().readValue(jsonBody.getJSONObject("metadata").toString(), new TypeReference<HashMap<String, String>>(){});

		String fileName = content.getOriginalFilename();
		File tmpFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
		content.transferTo(tmpFile);
		HttpHeaders responseHeaders = new HttpHeaders();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		try {
			responseHeaders.add("Content-Type", "text/plain");
			String stringResult = segmentator.segmentDocument(tmpFile);
			String body = "ERROR, the document has not been correctly segmented.";
			if(stringResult!=null) {
				responseHeaders.add("Content-Type", "application/xml");
				body = stringResult;
				status = HttpStatus.OK;
			}
			ResponseEntity<String> response = new ResponseEntity<String>(body, responseHeaders, status);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
}


