package de.hainet.datasegmentation;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.hainet.datasegmentation.modules.HTMLParser;
import de.hainet.datasegmentation.modules.PDFExtractor;
import de.hainet.datasegmentation.modules.TextClassification;
import de.hainet.datasegmentation.modules.TitleClassifier;
import de.hainet.datasegmentation.modules.TitleParser;

@Component
public class Segmentation {

	@Autowired
	HTMLParser parser;
	
	@Autowired
	TitleParser titleParser;
	
	@Autowired
	PDFExtractor extractor;
	
	@Autowired
	TitleClassifier titleClassifier;	
	
	@Autowired
	TextClassification textClassifier;	
	
	public String segmentDocument(File tmpFile) {
		JSONObject result = new JSONObject();
		// Convert PDF to HTML
		System.out.println("Starting segmentation...");
		System.out.println("PDF Extraction...");
		String html = extractor.extractPDFD(tmpFile);
		System.out.println("... PDF Extraction DONE");
		if(html==null) {
			String msg = "ERROR: returned null from PDFExtractor.";
			System.out.println(msg);
			return msg;
		}
//		return html;
//		// Analyse document to get titles and content.		
		System.out.println("Title parsing...");
		String html_with_titles = titleParser.parseTitles(html);
		System.out.println("... Title parsing DONE");
		if(html_with_titles==null) {
			String msg = "ERROR: returned null from titleParser.";
			System.out.println(msg);
			return msg;
		}
//		return html_with_titles;
//		// Classify sections (and titles?)
		System.out.println("Text Classification...");
		String html_classified = textClassifier.classifyText(html_with_titles);
		System.out.println("... Text Classification DONE");
		return html_classified;
	}
	
	public String segmentDocument_test(File tmpFile) {
		// Convert PDF to HTML
		extractor = new PDFExtractor();
		parser = new HTMLParser();
		titleParser = new TitleParser();
		textClassifier = new TextClassification();

		System.out.println("Starting segmentation...");
		System.out.println("PDF Extraction...");
		String html = extractor.extractPDFD(tmpFile);
		System.out.println("... PDF Extraction DONE");
//		return html;
//		// Analyse document to get titles and content.		
		System.out.println("Title parsing...");
		String html_with_titles = titleParser.parseTitles(html);
		System.out.println("... Title parsing DONE");
//		return html_with_titles;
//		// Classify sections (and titles?)
		String html_classified = textClassifier.classifyText(html_with_titles);
		return html_classified;

	}
	
	public static void main(String[] args) {
//		File f = new File("/Users/julianmorenoschneider/Downloads/Humane-AI-Net_Data/biomedical/555.pdf");
		File f = new File("/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/main2.pdf");
		Segmentation seg = new Segmentation();
		System.out.println("Output of Segementation Service: " + seg.segmentDocument_test(f));
	}

}