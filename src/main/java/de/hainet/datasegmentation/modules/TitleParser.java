package de.hainet.datasegmentation.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

@Component
public class TitleParser {

	public String parseTitles(String html) {
		try {
			String html_with_titles = "";
			
			File tempHTMLFile = File.createTempFile("Segmentation-InputTitle-", ".xml");
			tempHTMLFile.deleteOnExit();
			FileUtils.writeStringToFile(tempHTMLFile, html, "utf-8");
			File tempHTMLOutputFile = File.createTempFile("Segmentation-OutputTitle-", ".xml");
			tempHTMLOutputFile.deleteOnExit();
			
//			String cmd = "python3 /Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/titles2.py "+tempHTMLFile.getAbsolutePath()+" "+tempHTMLOutputFile.getAbsolutePath()+"";
			String cmd = "python3 Titles/titles2.py "+tempHTMLFile.getAbsolutePath()+" "+tempHTMLOutputFile.getAbsolutePath()+"";
//			String cmd = "python3 /Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/titles2.py --user /var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/Segmentation-9689498591007214272.html /var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/SegmentationOutput-10082226882180725254.html";
			String cmd1 = "pip3 install --user fuzzywuzzy pandas lxml regex";
			
			Runtime run = Runtime.getRuntime();
			Process pr1 = run.exec(cmd1);
//			printStream("LINE: ",pr1.getInputStream());
//			printStream("LINE ERROR: ",pr1.getErrorStream());
			
			Process pr = run.exec(cmd);
//			printStream("LINE: ",pr.getInputStream());
//			printStream("LINE ERROR: ",pr.getErrorStream());

			int exitCode = pr.waitFor();
		    if(exitCode==0) {
			    System.out.println("No errors should be detected");
		    }
		    else {
			    System.out.println("Exit code: "+exitCode);		    	
		    }
			html_with_titles = FileUtils.readFileToString(tempHTMLOutputFile,"utf-8");
//			System.out.println("HTML Output: "+html_with_titles);
			return html_with_titles;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void printStream(String prefix, InputStream stream) throws Exception {
		BufferedReader buf = new BufferedReader(new InputStreamReader(stream));
		String line = "";
		while ((line=buf.readLine())!=null) {
			System.out.println(prefix + line);
		}	
	}
	
	public static void main(String[] args) throws Exception {
		TitleParser tp = new TitleParser();
		String html = FileUtils.readFileToString(new File("/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/out/main.xml"),"utf-8");
//		System.out.println(html);
		System.out.println(html);
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		System.out.println("---------------------------------");
		String res = tp.parseTitles(html);
		System.out.println("RESULT OF TITLE PARSER: " + res);
//		String cmd = "python3 /Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/titles2.py --user /var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/Segmentation-9689498591007214272.html /var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/SegmentationOutput-10082226882180725254.html";
//		String cmd1 = "pip3 install --user fuzzywuzzy pandas lxml regex";
//		
//		Runtime run = Runtime.getRuntime();
//		Process pr1 = run.exec(cmd1);
//		printStream("LINE: ",pr1.getInputStream());
//		printStream("LINE ERROR: ",pr1.getErrorStream());
//		
//		Process pr = run.exec(cmd);
//		printStream("LINE: ",pr.getInputStream());
//		printStream("LINE ERROR: ",pr.getErrorStream());
//
//		int exitCode = pr.waitFor();
//	    System.out.println("Exit code: "+exitCode);
//	    if(exitCode==0) {
//		    System.out.println("No errors should be detected");
//	    }
//		String html_with_titles = FileUtils.readFileToString(new File("/var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/SegmentationOutput-10082226882180725254.html"),"utf-8");
//		System.out.println("HTML Output: "+html_with_titles);
//		
////	    ProcessBuilder processBuilder = new ProcessBuilder("python3", 
////	    		"/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/titles2.py",
////	    		"/var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/Segmentation-9689498591007214272.html",
////	    		"/var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/SegmentationOutput-10082226882180725254.html");
////	    processBuilder.redirectErrorStream(true);
////
////	    ProcessBuilder processBuilder2 = new ProcessBuilder("pip3" , "install", "fuzzywuzzy");
////	    processBuilder2.start();
////
////	    Process process = processBuilder.start();
////		BufferedReader buf = new BufferedReader(new InputStreamReader(process.getInputStream()));
////		String line = "";
////		while ((line=buf.readLine())!=null) {
////			System.out.println("LINE: " + line);
////		}
//
////		String html_with_titles = FileUtils.readFileToString(new File("/var/folders/x8/xv5pnz2n4mx74j0dvj8p5nyr0000gp/T/SegmentationOutput-10082226882180725254.html"),"utf-8");
//		System.out.println("HTML Output: "+html_with_titles);
//
////	    int exitCode = process.waitFor();
////	    if(exitCode==0) {
////		    System.out.println("No errors should be detected");
////	    }

	}
	
}
