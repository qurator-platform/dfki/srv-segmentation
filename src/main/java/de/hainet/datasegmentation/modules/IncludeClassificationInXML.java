package de.hainet.datasegmentation.modules;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class IncludeClassificationInXML {

	public static void main(String[] args) throws Exception {
		String path = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/tesseract_titled/";
//		String path2 = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/classification/";
		String path2 = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/class_input/";
		String pathOutput = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/classified/";

		File folder = new File(path);
		File files[] = folder.listFiles();
		for (File file : files) {
			if(file.getName().contains(".xml")) {
				
				File f1 = new File(path2 + file.getName().replace(".xml", "_texts.txt"));
				File f2 = new File(path2 + file.getName().replace(".xml", "_texts.cls.result.json"));
				JSONArray textsArray = new JSONArray(FileUtils.readFileToString(f1, "utf-8"));
				JSONArray labelsArray = new JSONArray(FileUtils.readFileToString(f2, "utf-8"));
				
				System.out.println(file.getName());
				List<String> titles = new LinkedList<String>();
				List<String> texts = new LinkedList<String>();
				JSONArray array = new JSONArray();
				String content = FileUtils.readFileToString(file,"utf-8");
				Document doc = Jsoup.parse(content);
				Elements divs = doc.select("subsection");
				for (Element div : divs) {
					System.out.println("\t------------------------------------");
					String title = "";
//					Elements ts = div.getElementsByTag("title").get(0).getElementsByTag("p");
					Elements ts = div.getElementsByTag("title");
					for (Element t : ts) {
						title += " " + t.text();
					}
					title = title.replace("\n", " ").replace("<p>", "").replace("</p>", "").trim();
					
					String text = ""; 
					Elements ps = div.getElementsByTag("p");
					for (Element p : ps) {
						text += " " + p.text();
					}
					text = text.trim();
					
					// TODO Search text in texts and find passing label.
					for (int i = 0; i < textsArray.length(); i++) {
						String t = textsArray.getString(i);
						if(text.equalsIgnoreCase(t)) {
							//TODO Assign label in position i to the subsection Element in XML.
							String label = labelsArray.getJSONArray(i).getString(1);
							div.attr("classification", label);
							System.out.println(title + "-->" + label);
						}
						else {
							
						}
					}
					
//					System.out.println("\t"+title);
//					System.out.println("\t"+text);
					
//					if(div.hasAttr("style")) {
////						System.out.println("---------------------");
////						System.out.println("---------------------");
//						Elements ps = div.getElementsByTag("p");
//						for (Element p : ps) {
//							//System.out.println(p);
//							String text = p.text();
////							String heading1Regex = "^(\\d+)\\.(\\d\\.)?\\s(?<title>.*?)$";
//							String heading1Regex = "^([viVI\\d]+)\\.?([viVI\\d]\\.?)?\\s(?<title>.*?)$";
//
//							if(text.matches(heading1Regex)) {
//								if(text.indexOf(".", text.indexOf(" ")+1)!=-1) {
//									titles.add(text.substring(0,text.indexOf(".", text.indexOf(" ")+1)));
//								}
//								else {
//									titles.add(text);
//								}
//							}
//							
//						}
//					}
				}
//				System.out.println(doc.toString());
				FileUtils.writeStringToFile(new File(pathOutput + file.getName()), doc.toString(), "utf-8");
//				break;
			}
		}
		

	}

}
