package de.hainet.datasegmentation.modules;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.hainet.datasegmentation.tree.TNode;

@Component
public class HTMLParser {

	@Autowired
	TitleClassifier classifier;
	
	public HTMLParser() {
		classifier = new TitleClassifier();
	}
	
	public JSONObject parseHTML(String content) {
		JSONObject json = new JSONObject();
		JSONObject sections = new JSONObject();
		String typeOfValue = "None";
		String oldKey = null;
		boolean first = true;
		
		TNode tree = new TNode(new LinkedList<TNode>(), null, "filename", "filename");
		
		Document doc = Jsoup.parse(content);
		Elements divs = doc.select("div");
		for (Element div : divs) {
			if(div.hasAttr("style")) {
				Elements ps = div.getElementsByTag("p");
				for (Element p : ps) {
					String text = p.text();
					//String heading1Regex = "^(\\d+)\\.(\\d\\.)?\\s(?<title>.*?)$";
					String heading1Regex = "^([viVI\\d]+)\\.?([viVI\\d]\\.?)?\\s(?<title>.*?)$";
					if(text.matches(heading1Regex)) {
						text = text.toLowerCase();
						if(first && text.indexOf("introduction")==-1) {
							continue;
						}
						String introRegex = "^([viVI\\d]+)\\.?([viVI\\d]\\.?)?\\s(introduction)\\s?\\d*?$";
						if(first && !text.matches(introRegex)) {
							continue;
						}
						if(first==true && (text.length()<6 || text.contains("{") || text.contains("et al") || text.contains(",")) ) {
							continue;
						}
						else if(text.length()<6 || text.contains("{") || text.contains("et al")) {
							continue;
						}
						String k = text.substring(0, text.indexOf(" "));
						String v = text.substring(text.indexOf(" "));					
						String type = getType(k);
						typeOfValue = setTypeOfValue(type,typeOfValue);
						if(!type.equalsIgnoreCase(typeOfValue)) {
							continue;
						}
						if(first) {
							oldKey = k;
							TNode node = new TNode(new LinkedList<TNode>(), null, k, v);
							List<String> classification = classifier.classifyTitle(v);
							node.setClassification(classification.toString());
							tree.addTNode(node);
							first = false;
						}
						else if (nextValue(oldKey, k, typeOfValue)) {
							oldKey = k;
							TNode node = new TNode(new LinkedList<TNode>(), null, k, v);
							List<String> classification = classifier.classifyTitle(v);
							node.setClassification(classification.toString());
							tree.addTNode(node);
							first = false;
						}
						else if (isChild(oldKey, k, typeOfValue)) {
							TNode node = new TNode(new LinkedList<TNode>(), null, k, v);
							List<String> classification = classifier.classifyTitle(v);
							node.setClassification(classification.toString());
							tree.addChildToTNode(node,oldKey);
							first = false;
						}
						else {
//							System.out.println("\tNOT INCLUDED.");
						}
					}
				}
			}
		}
		List<TNode> nodes = tree.getChilds();
		for (TNode tNode : nodes) {
			JSONObject tJSON = tNode.getJSONObject();			
			sections.put(tNode.getKey(),tJSON);
		}
		json.put("sections", sections);
		return json;
	}
	
	private static String getType(String k) {
		if(k.matches("^([\\d]+)\\.([\\d]\\.)?$")) {
			return "NP";
		}
		else if(k.matches("^([\\d]+)\\.?([\\d]\\.?)?$")) {
			return "N";
		}
		else if(k.matches("^([viVI]+)\\.([viVI]\\.)?$")) {
			return "RP";
		}
		else if(k.matches("^([viVI]+)\\.?([viVI]\\.?)?$")) {
			return "R";
		}
		else {
			return "O";
		}
	}
	
	private String setTypeOfValue(String type, String typeOfValue) {
		if(typeOfValue.equalsIgnoreCase("None")) {
			return type;
		}
		return typeOfValue;
	}

	public static boolean nextValue(String oldKey, String newKey, String type) {
		if(type.equalsIgnoreCase("N") || type.equalsIgnoreCase("NP")) {
			int ok = (oldKey.contains("."))?Integer.parseInt(oldKey.substring(0, oldKey.indexOf('.'))):Integer.parseInt(oldKey);
			int nk = (newKey.contains("."))?Integer.parseInt(newKey.substring(0, newKey.indexOf('.'))):Integer.parseInt(newKey);
			if(nk==ok+1) {
				return true;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("R") || type.equalsIgnoreCase("RP")) {
			oldKey = oldKey.substring(0, oldKey.indexOf('.'));
			newKey = newKey.substring(0, newKey.indexOf('.'));
			if(oldKey.equalsIgnoreCase("i")) {
				return (newKey.equalsIgnoreCase("ii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("ii")) {
				return (newKey.equalsIgnoreCase("iii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("iii")) {
				return (newKey.equalsIgnoreCase("iv"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("iv")) {
				return (newKey.equalsIgnoreCase("v"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("v")) {
				return (newKey.equalsIgnoreCase("vi"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("vi")) {
				return (newKey.equalsIgnoreCase("vii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("vii")) {
				return (newKey.equalsIgnoreCase("viii"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("viii")) {
				return (newKey.equalsIgnoreCase("ix"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("ix")) {
				return (newKey.equalsIgnoreCase("x"))?true:false;
			}
			else if(oldKey.equalsIgnoreCase("x")) {
				return (newKey.equalsIgnoreCase("xi"))?true:false;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("O")) {
			return false;
		}
		else {
			System.out.println("ERROR: Type not supported.");
			return false;
		}
	}
	
	public static boolean isChild(String oldKey, String newKey, String type) {
		if(oldKey.equalsIgnoreCase(newKey)) {
			return false;
		}
		if(type.equalsIgnoreCase("N") || type.equalsIgnoreCase("NP")) {
			if(newKey.startsWith(oldKey)) {
				return true;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("R") || type.equalsIgnoreCase("RP")) {
			if(newKey.startsWith(oldKey)) {
				return true;
			}
			else {
				return false;
			}
		}
		else if(type.equalsIgnoreCase("O")) {
			return false;
		}
		else if(type.equalsIgnoreCase("M")) {
			return false;
		}
		else {
			System.out.println("ERROR: Type not supported.");
			return false;
		}
	}

}
