package de.hainet.datasegmentation.modules;

import java.io.File;
import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.MultipartBody;

@Component
public class PDFExtractor {

	@Value( "${tesseract.hostname}" )
	String tesseractHostname = "localhost";
	@Value( "${tesseract.port}" )
	String tesseractPort = "5000";
	
	public String extractPDFD(File file) {
		try {
			String response = null;
			// Use Tesseract instead of PDFBox.
			File tempHTMLFile = File.createTempFile("Segmentation-", ".html");
			tempHTMLFile.deleteOnExit();
			
			String host = null;
			if(tesseractHostname.equalsIgnoreCase("qtess")) {
				InetAddress address = InetAddress.getByName("qtess"); 
				System.out.println("INET ADDRESS: " + address.getHostAddress()); 
				host = address.getHostAddress();
			}
			else {
				host = tesseractHostname;
			}

			Unirest.setTimeouts(0, 0);
			System.out.println("Accessing:");
			System.out.println("http://" + host + ":" + tesseractPort + "/tesseract-recognize/process");
			//MultipartBody hrwb = Unirest.post(tesseractHostname + ":" + tesseractPort + "/tesseract-recognize/process")
			MultipartBody hrwb = Unirest.post("http://" + host + ":" + tesseractPort + "/tesseract-recognize/process")
	    			.field("images", file)
	    			.field("options", "[\"--layout\", \"region\"]");
	    	HttpResponse<String> httpResponse = hrwb.asString();

//			URL url = new URL(tesseractHostname + ":" + tesseractPort + "/tesseract-recognize/process");
//			HttpURLConnection con = (HttpURLConnection) url.openConnection();
//			con.setRequestMethod("POST");
//				    	
	    	
//			String cmd = "curl -o "+tempHTMLFile.getAbsolutePath()+" -F options='[\"--layout\",\"region\"]' -F images=@"+file.getAbsolutePath()+" http://localhost:5000/tesseract-recognize/process";
//			System.out.println(cmd);
//			Runtime run = Runtime.getRuntime();
//			Process pr = run.exec(cmd);
//			pr.waitFor();
//			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
//			String line = "";
//			while ((line=buf.readLine())!=null) {
//				System.out.println(line);
//			}
//			BufferedReader bufe = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
//			String linee = "";
//			while ((linee=bufe.readLine())!=null) {
//				System.out.println(linee);
//			}
//			
//			String s = FileUtils.readFileToString(tempHTMLFile, "utf-8");
//			System.out.println(s);

	    	if(httpResponse.getStatus()==200) {
				System.out.println("File correctly processed by Tesseract.");
	    		response = httpResponse.getBody();
	    	}
	    	else {
				System.out.println("ERROR processing Tesseract.");
	    	}
	    	
//			PDDocument doc = PDDocument.load(file);
//			parsedText = new PDFText2HTML().getText(doc);
//			String htmlContent = FileUtils.readFileToString(tempHTMLFile,"utf-8");
//			return htmlContent;
	    	return response;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		
		PDFExtractor pe = new PDFExtractor();
		pe.extractPDFD(new File("/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/main2.pdf"));
		
	}
}
