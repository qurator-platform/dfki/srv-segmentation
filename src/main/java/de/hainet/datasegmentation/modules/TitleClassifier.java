package de.hainet.datasegmentation.modules;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TitleClassifier {
	
	List<String> introduction = Arrays.asList("introduction");
	List<String> discussion = Arrays.asList("discussion","analysis","discussion and conclusions");
	List<String> relatedWork = Arrays.asList("related work","background","literature");
	List<String> acks = Arrays.asList("acknowledgements","acknowledgments");
	List<String> conclusions = Arrays.asList("conclusions", 
			"conclusion", 
			"summary", 
			"discussion and conclusions", 
			"concluding remarks",
			"conclusions and outlook",
			"outlook",
			"conclusion and outlook",
			"conclusion and discussion",
			"conclusions and perspectives",
			"summary and conclusion",
			"summary and conclusions",
			"discussion and conclusion",
			"conclusion and future work"
			);
	List<String> results = Arrays.asList(
			"results", 
			"results and discussion", 
			"numerical results", 
			"main results",
			"result",
			"experiments and results",
			"experimental results");
	List<String> methods = Arrays.asList(
			"methods", 
			"materials and methods", 
			"methodology",
			"method",
			"proof of theorem",
			"algorithm",
			"problem formulation");
	List<String> experiments = Arrays.asList(
			"experiments", 
			"numerical experiments", 
			"experiments and results", 
			"analysis");
	List<String> observations = Arrays.asList(
			"observations", 
			"analysis");
	List<String> references = Arrays.asList(
			"references", 
			"bibliography");
	List<String> evaluation = Arrays.asList(
			"evaluation", 
			"experimental evaluation");
	List<String> approach = Arrays.asList(
			"use case", 
			"use cases", 
			"system model", 
			"data",
			"examples",
			"notation",
			"theory");
	/**
	 preliminaries::: 7
	 observations and data reduction::: 3
	 systematics::: 2
	*/
	
	public List<String> classifyTitle(String title) {
		title = title.trim();
		List<String> types = new LinkedList<String>();
		if(this.introduction.contains(title)) {
			types.add("introduction");
		}
		if(this.acks.contains(title)) {
			types.add("acks");
		}
		if(this.approach.contains(title)) {
			types.add("approach");
		}
		if(this.conclusions.contains(title)) {
			types.add("conclusions");
		}
		if(this.discussion.contains(title)) {
			types.add("discussion");
		}
		if(this.evaluation.contains(title)) {
			types.add("evaluation");
		}
		if(this.experiments.contains(title)) {
			types.add("experiments");
		}
		if(this.methods.contains(title)) {
			types.add("methods");
		}
		if(this.observations.contains(title)) {
			types.add("observations");
		}
		if(this.references.contains(title)) {
			types.add("references");
		}
		if(this.relatedWork.contains(title)) {
			types.add("relatedwork");
		}
		if(this.results.contains(title)) {
			types.add("results");
		}
		if(types.isEmpty()) {
			// extra method to identify elements. Slower but with more coverage.
			types = classifyTitleByParts(title);
		}
		return types;
	}
	
	public List<String> classifyTitleByParts(String title) {
		title = title.trim();
		List<String> types = new LinkedList<String>();
		for (String s : this.introduction) {
			if(title.contains(s)) {
				types.add("introduction");
				break;
			}
		}
		for (String s : this.acks) {
			if(title.contains(s)) {
				types.add("acks");
				break;
			}
		}
		for (String s : this.approach) {
			if(title.contains(s)) {
				types.add("approach");
				break;
			}
		}
		for (String s : this.conclusions) {
			if(title.contains(s)) {
				types.add("conclusions");
				break;
			}
		}
		for (String s : this.discussion) {
			if(title.contains(s)) {
				types.add("discussion");
				break;
			}
		}
		for (String s : this.evaluation) {
			if(title.contains(s)) {
				types.add("evaluation");
				break;
			}
		}
		for (String s : this.experiments) {
			if(title.contains(s)) {
				types.add("experiments");
				break;
			}
		}
		for (String s : this.methods) {
			if(title.contains(s)) {
				types.add("methods");
				break;
			}
		}
		for (String s : this.observations) {
			if(title.contains(s)) {
				types.add("observations");
				break;
			}
		}
		for (String s : this.references) {
			if(title.contains(s)) {
				types.add("references");
				break;
			}
		}
		for (String s : this.relatedWork) {
			if(title.contains(s)) {
				types.add("relatedwork");
				break;
			}
		}
		for (String s : this.results) {
			if(title.contains(s)) {
				types.add("results");
				break;
			}
		}
		if(types.isEmpty()) {
			types.add("None");
		}
		return types;
	}

}
