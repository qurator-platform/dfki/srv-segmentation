package de.hainet.datasegmentation.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
public class TextClassification {

	
	public static void main2(String[] args) throws Exception {
		String path = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/tesseract_titled/";
//		String path2 = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/classification/";
		String path2 = "/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/class_input/";
		File folder = new File(path);
		File files[] = folder.listFiles();
		for (File file : files) {
			if(file.getName().contains(".xml")) {
				System.out.println(file.getName());
				List<String> titles = new LinkedList<String>();
				List<String> texts = new LinkedList<String>();
				JSONArray array = new JSONArray();
				String content = FileUtils.readFileToString(file,"utf-8");
				Document doc = Jsoup.parse(content);
				Elements divs = doc.select("subsection");
				for (Element div : divs) {
					System.out.println("\t------------------------------------");
					String title = "";
//					Elements ts = div.getElementsByTag("title").get(0).getElementsByTag("p");
					Elements ts = div.getElementsByTag("title");
					for (Element t : ts) {
						title += " " + t.text();
					}
					title = title.replace("\n", " ").replace("<p>", "").replace("</p>", "").trim();
					
					String text = ""; 
					Elements ps = div.getElementsByTag("p");
					for (Element p : ps) {
						text += " " + p.text();
					}
					text = text.trim();
					titles.add(title);
					texts.add(text);
					array.put(text);
//					System.out.println("\t"+title);
//					System.out.println("\t"+text);
					
//					if(div.hasAttr("style")) {
////						System.out.println("---------------------");
////						System.out.println("---------------------");
//						Elements ps = div.getElementsByTag("p");
//						for (Element p : ps) {
//							//System.out.println(p);
//							String text = p.text();
////							String heading1Regex = "^(\\d+)\\.(\\d\\.)?\\s(?<title>.*?)$";
//							String heading1Regex = "^([viVI\\d]+)\\.?([viVI\\d]\\.?)?\\s(?<title>.*?)$";
//
//							if(text.matches(heading1Regex)) {
//								if(text.indexOf(".", text.indexOf(" ")+1)!=-1) {
//									titles.add(text.substring(0,text.indexOf(".", text.indexOf(" ")+1)));
//								}
//								else {
//									titles.add(text);
//								}
//							}
//							
//						}
//					}
				}
//				FileUtils.writeLines(new File(path2 + file.getName().replace(".xml", "_titles.txt")), "utf-8", titles);
//				FileUtils.writeLines(new File(path2 + file.getName().replace(".xml", "_texts.txt")), "utf-8", texts);
				FileUtils.writeStringToFile(new File(path2 + file.getName().replace(".xml", "_texts.txt")), array.toString(), "utf-8");
//				break;
			}
		}
		

	}

	
	public JSONArray classifyText2JSON(String content) {
		List<String> titles = new LinkedList<String>();
		List<String> texts = new LinkedList<String>();
		JSONArray array = new JSONArray();

		Document doc = Jsoup.parse(content);
		Elements divs = doc.select("subsection");
		for (Element div : divs) {
//			System.out.println("\t------------------------------------");
			String title = "";
//			Elements ts = div.getElementsByTag("title").get(0).getElementsByTag("p");
			Elements ts = div.getElementsByTag("title");
			for (Element t : ts) {
				title += " " + t.text();
			}
			title = title.replace("\n", " ").replace("<p>", "").replace("</p>", "").trim();
			
			String text = ""; 
			Elements ps = div.getElementsByTag("p");
			for (Element p : ps) {
				text += " " + p.text();
			}
			text = text.trim();
			titles.add(title);
			texts.add(text);
			array.put(text);
		}
		return array;		
	}

	public JSONArray getTextArray(String content) {
		List<String> titles = new LinkedList<String>();
		List<String> texts = new LinkedList<String>();
		JSONArray array = new JSONArray();
		Document doc = Jsoup.parse(content);
		Elements divs = doc.select("subsection");
		for (Element div : divs) {
//			System.out.println("\t------------------------------------");
			String title = "";
//			Elements ts = div.getElementsByTag("title").get(0).getElementsByTag("p");
			Elements ts = div.getElementsByTag("title");
			for (Element t : ts) {
				title += " " + t.text();
			}
			title = title.replace("\n", " ").replace("<p>", "").replace("</p>", "").trim();
			
			String text = ""; 
			Elements ps = div.getElementsByTag("p");
			for (Element p : ps) {
				text += " " + p.text();
			}
			text = text.trim();
			titles.add(title);
			texts.add(text);
			array.put(text);
		}
		return array;
	}
	
	public JSONArray getLabels(JSONArray textsArray) {
		JSONArray labelsArray = new JSONArray();			
		try {
			// Call python classification script			
			File tempTextsFile = File.createTempFile("Segmentation-", ".txt", new File("test_texts"));
			FileUtils.writeStringToFile(tempTextsFile, textsArray.toString(), "utf-8");
			tempTextsFile.deleteOnExit();
//			File tempLabelsFile = File.createTempFile("SegmentationOutput-", ".json");
	//		tempLabelsFile.deleteOnExit();
			
//			String cmd = "python3 " + 
//					"/Users/julianmorenoschneider/Downloads/HAINET_experiments/SNC/src/train.py " + 
//					"-mode test_text " + 
//					"-test_from /Users/julianmorenoschneider/Downloads/HAINET_experiments/SNC/models/CLS_pubmedbertB_ep3tb16eb50ws500ls2000wd0.01/checkpoint-72000 " + 
//					"-text_path test_texts";
			String cmd = "python3 " + 
					"SNC/src/train.py " + 
					"-mode test_text " + 
					"-test_from SNC/models/CLS_pubmedbertB_ep3tb16eb50ws500ls2000wd0.01/checkpoint-72000 " + 
					"-text_path test_texts";
//			System.out.println(cmd);
	//		String cmd = "python3 /Users/julianmorenoschneider/Downloads/HAINET_experiments/SNC/titles2.py "+tempHTMLFile.getAbsolutePath()+" "+tempHTMLOutputFile.getAbsolutePath()+"";
//			String cmd1 = "pip3 install --user -r /Users/julianmorenoschneider/Downloads/HAINET_experiments/SNC/requirements.txt";
			String cmd1 = "pip3 install --user sklearn";
			
			Runtime run = Runtime.getRuntime();
			Process pr1 = run.exec(cmd1);
			printStream("LINE: ",pr1.getInputStream());
			printStream("LINE ERROR: ",pr1.getErrorStream());
			
			Process pr = run.exec(cmd);
			printStream("LINE: ",pr.getInputStream());
			printStream("LINE ERROR: ",pr.getErrorStream());
	
			int exitCode = pr.waitFor();
		    if(exitCode==0) {
			    System.out.println("No errors should be detected");
			    File f = new File(tempTextsFile.getAbsolutePath().replace(".txt", ".cls.result.json"));
				labelsArray = new JSONArray(FileUtils.readFileToString(f, "utf-8"));
//				System.out.println("LABELS: ");
//				System.out.println(labelsArray.toString(1));
				
				File dir = new File("test_texts/DONE");
				if(dir.exists() && dir.isDirectory()) {
					dir.delete();
				}
				File dirFiles = new File("test_texts");
				File files[] = dirFiles.listFiles();
				for (File file : files) {
					if(file.getName().startsWith("Segmentation")) {
						file.delete();
					}
				}
				
				return labelsArray;
		    }
		    else {
			    System.out.println("Exit code: "+exitCode);
			    return null;
		    }		    
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
		
	public static void printStream(String prefix, InputStream stream) throws Exception {
		BufferedReader buf = new BufferedReader(new InputStreamReader(stream));
		String line = "";
		while ((line=buf.readLine())!=null) {
			System.out.println(prefix + line);
		}	
	}
	
	public String classifyText(String content) {		
		// Include Classification in the XML file.
		JSONArray textsArray = getTextArray(content);
		JSONArray labelsArray = getLabels(textsArray);			
		Document doc = Jsoup.parse(content);
		Elements divs = doc.select("subsection");
		for (Element div : divs) {
			System.out.println("\t------------------------------------");
			String title = "";
//				Elements ts = div.getElementsByTag("title").get(0).getElementsByTag("p");
			Elements ts = div.getElementsByTag("title");
			for (Element t : ts) {
				title += " " + t.text();
			}
			title = title.replace("\n", " ").replace("<p>", "").replace("</p>", "").trim();

			String text = ""; 
			Elements ps = div.getElementsByTag("p");
			for (Element p : ps) {
				text += " " + p.text();
			}
			text = text.trim();

			// TODO Search text in texts and find passing label.
			for (int i = 0; i < textsArray.length(); i++) {
				String t = textsArray.getString(i);
				if(text.equalsIgnoreCase(t)) {
					//TODO Assign label in position i to the subsection Element in XML.
					String label = labelsArray.getJSONArray(i).getString(1);
					div.attr("classification", label);
					System.out.println(title + "-->" + label);
				}
				else {

				}
			}
		}
		return doc.toString();
	}
	
	public static void main(String[] args) throws Exception {
		
		TextClassification tc = new TextClassification();
		String content = FileUtils.readFileToString(new File("/Users/julianmorenoschneider/Downloads/HAINET_experiments/processed/out/main3.xml"), "utf-8");
		System.out.println(content);
		String s = tc.classifyText(content);
		System.out.println("------------------");
		System.out.println("------------------");
		System.out.println("------------------");
		System.out.println("------------------");
		System.out.println(s);
		
	}
	
}
