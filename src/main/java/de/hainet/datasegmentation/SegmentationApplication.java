package de.hainet.datasegmentation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.hainet.datasegmentation"})
public class SegmentationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SegmentationApplication.class, args);
    } 

}
