package de.hainet.datasegmentation.tree;

import java.util.List;

import org.json.JSONObject;

public class TNode {

	List<TNode> childs;
	TNode parent;
	
	String key;
	String value;
	String classification;
	
	public TNode(List<TNode> childs, TNode parent, String key, String value) {
		super();
		this.childs = childs;
		this.parent = parent;
		this.key = key;
		this.value = value;
	}
	
	public void addTNode(TNode node) {
		node.parent = this;
		childs.add(node);
	}
	
	public List<TNode> getChilds(){
		return childs;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void printNode(String tab, String space) {
		System.out.println(tab + key + "::" + value);
		for (TNode tNode : childs) {
			tNode.printNode(tab+space,space);
		}
	}
	
	public boolean hasChilds() {
		return !childs.isEmpty();
	}

	public void addChildToTNode(TNode node, String oldKey) {
		for (TNode tNode : childs) {
			if(tNode.key.equalsIgnoreCase(oldKey)) {
				tNode.addTNode(node);
			}
		}
	}

	public JSONObject getJSONObject() {
		JSONObject json = new JSONObject();
		json.put("text", this.value);
		json.put("class", this.classification);
		JSONObject childs = new JSONObject();
		if(!this.childs.isEmpty()) {
			for (TNode tNode : this.childs) {
				childs.put(tNode.getKey(), tNode.getJSONObject());
			}
			json.put("subtitles", childs);
		}
		return json;
	}
}
