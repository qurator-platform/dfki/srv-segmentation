# -*- coding: utf-8 -*-

import re
from fuzzywuzzy import fuzz
from fuzzywuzzy.process import dedupe
import pandas as pd
import os
import lxml.etree as et
import csv
import string
import regex
import sys

### apply rules to title candidates:
### - remove candidates with key words for non-title info
### - remove repeating candidates
### - filter out weird capitalization
### - cut everything after references
def filter_by_rules(candidates):
    
    taboo_words = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 
             'september', 'october', 'november', 'december', 'date', 
             'jan', 'feb', 'mar', 'apr', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec',
             'figure', 'fig.', 'table', 'tab.', 'page', 't']
    taboo_phrases = ['the proof of', 'et al.']
    
    good = []
    bad = []
    
    # remove repeating candidates
    for (i, candidate) in enumerate(candidates):
        similars = []
        similars.append(candidate)
        for (j, other_candidate) in enumerate(candidates[i+1:]):
            if fuzz.ratio(candidate[0], other_candidate[0]) >= 85:
                similars.append(other_candidate)
        if len(similars) > 2:
            for s in similars:
                candidates.remove(s)
    
    for candidate in candidates:
        
        text = candidate[0]
        c_type = candidate[1]
        
        # cut everything after references
        if 'references' in text.lower():
            good.append(candidate)
            break
        
        # remove candidates containing at least half of non-alphabetic characters
        if len(re.sub(r'[A-Za-z]+' ,'', text)) >= len(text)/2 or \
        len(text) - len(re.sub(r'[,;:]', '', text)) > 2:
            bad.append(candidate)
            continue
        
        clean_text = ''
        if c_type in ['num', 'gen']:
            clean_text = re.sub(r'[—\-:\d\']', '', text)
        
        # remove sentences and list items
        # remove candidates with taboo words and phrases
        # remove candidates with weird capitalization
        if not (text.endswith('.') or text.endswith(',') or text.endswith(';') or text.endswith(':')) and \
        not any(word in taboo_words for word in text.lower().split()) and \
        not any(phrase in text.lower() for phrase in taboo_phrases):
            if clean_text:
                if string.capwords(clean_text).istitle():
                    good.append(candidate)
                else:
                    bad.append(candidate)
            else:
                good.append(candidate)
        else:
            bad.append(candidate)
    
    return good, bad

### extract 
### (1) numbered candidates by length and regexp
### (2) unnumbered candidates by length, starting with capital letter 
def extract_candidates(tree, prefix):

    doc_candidates = []

    root = tree.getroot()
    pages = root.findall(prefix+'Page')

    for page in pages:
        text_regions = page.findall(prefix+'TextRegion')

        for text_region in text_regions:
            text_equiv = text_region.find(prefix+'TextEquiv')
            unicode = text_equiv.find(prefix+'Unicode')
            text = unicode.text
                
            if len(text)>5 and len(text)<100:
                # find candidates numerated with a dot (very reliable) 
                if regex.match(r'^([\dA-Za-z]{1,4}\.)+([\dA-Z]{1,4})?\s[A-Z].+', text):
                    doc_candidates.append([re.sub(os.linesep,' ',text), 'numdot', unicode])
                # find other numerated candidates
                elif regex.match(r'^[\dA-Za-z]{1,4}\s[A-Z].+', text):
                    doc_candidates.append([re.sub(os.linesep,' ',text), 'num', unicode])
                # find unnumerated candidates (filtered by length)
                elif regex.match(r'^[A-Z].+', text):
                    doc_candidates.append([re.sub(os.linesep,' ',text), 'gen', unicode])

    doc_candidates, trash = filter_by_rules(doc_candidates)
    
    return doc_candidates

def get_levels(title_elems, title_types):
    
    levels = {
        'num': -1,
        'cap_letter': -1,
        'letter': -1,
        'roman_num': -1,
        'cap': -1,
        'word': -1
    }
    
    level_priority = ['num', 'roman_num', 'cap_letter', 'letter', 'cap', 'word']
    
    list_capless = ['num', 'cap_letter', 'letter', 'roman_num']
    
    elem_levels = {}
    
    max_level = -1
    group = ''
    prev_group = ''
    prev_prev_group = ''
    
    for i, elem in enumerate(title_elems):
        
        text = elem.text
        
        upper = False
        if text.isupper():
            upper = True
            
        first = text.split()[0]
        
        match = re.match(r'(?P<num>^(\d{1,2}\.)*\d{1,2}\.?$)|(?P<cap_letter>^([A-H]\.)*[A-H]\.?$)|(?P<letter>^([a-z]\.)*[a-z]\.?$)|(?P<roman_num>^([IlVX]{1,4}\.)*[IlVX]{1,4}\.?$)|(?P<cap>^[A-Z\-:]+$)|(?P<word>^[\w\-:]+$)', first)
        
        if not match:
            continue
        
        if not group:
            group = match.lastgroup
            if group == 'cap' and not upper:
                group = 'word'
            max_level += 1
            levels[group] = max_level
        else:
            if prev_group:
                prev_prev_group = prev_group
            prev_group = group
            group = match.lastgroup
            if group == 'cap' and not upper:
                group = 'word'
                        
            if group != prev_group:
                
                # add new level
                if levels[group] == -1:
                    max_level += 1
                    levels[group] = max_level
        
        # assign levels from dict to each elem
        # in case of number increase level by number of dots        
        dots = 0
        max_dots = 0
        if group == 'num' or group == 'numdot':
            dots = first.rstrip('.').count('.')
            
            if dots > max_dots:
                num_lvl = levels[group]
                for group_name in level_priority:
                    if levels[group_name] > levels[group]:
                        levels[group_name] += dots - max_dots
                max_dots = dots
                        
        elem_levels[elem] = [group, dots]
        
        max_level = max(levels[group] + dots, max_level)
                
    
    elems = []
    groups = []
    values = []
    for elem in title_elems:
        try:                
            group, dots = elem_levels[elem]
            elem_levels[elem] = levels[group] + dots
            elems.append(elem)
            groups.append(group)
            values.append(levels[group] + dots)
        except:
            continue
    
    # remove candidates breaking a numerized titles sequence
    for i, g in enumerate(groups):
        
        if g == 'word' or g == 'cap':
            
            j=i-1
            while j > 0 and groups[j] not in list_capless:
                j -= 1
                
            if j > 0:
                k = i
                while k < len(values) and groups[k] not in list_capless:
                    k += 1
                
                if k < len(values):
                    elem_levels[elems[i]] = -2
    
    # shift levels back in case previous levels were removed
    for i in range(0, max(elem_levels.values())+1):
        while i<max(elem_levels.values()) and i not in elem_levels.values():
            for k, v in elem_levels.items():
                if v > i:
                    elem_levels[k] -= 1

        
    return elem_levels, max_level

def insert_title(tree, level, prefix):
    
    regions = tree.findall("{p}Unicode[@title='{lvl}']".format(p=prefix, lvl=level))
    
    for region in regions:
        
        title = et.Element("title")
        title.set('level', str(level))
        region.addprevious(title)
        title.insert(0, region)   # this moves the region into title tag
        
        section = et.Element("subsection")
        section.set('level', str(level))
        title.addprevious(section)
        section.insert(0, title)   # this moves the title into region tag
         
        current = section.getnext()
        
        while current is not None and 'title' not in current.attrib:
            nxt = current.getnext()
            section.append(current)
            current = nxt

def create_xml(output_file, tree, max_level, prefix):
    
    xmlstr = et.tostring(tree, encoding='unicode', method='xml')
    
    et.strip_tags(tree,'{p}TextRegion'.format(p=prefix),'{p}TextEquiv'.format(p=prefix),'{p}Coords'.format(p=prefix))             
    
    for level in range(max_level,-1,-1):
        insert_title(tree, level, prefix)
        
    for elem in tree.iter():
        if elem.tag == "{p}Unicode".format(p=prefix):
            elem.tag = 'p'
            if 'title' in elem.attrib:
                elem.attrib.pop('title')

    print("Output_file: "+output_file)
    tree.write(output_file)
    
    # create titles-only structure
    et.strip_tags(tree, 'subsection', '{p}Metadata'.format(p=prefix), '{p}Creator'.format(p=prefix), \
                  '{p}Created'.format(p=prefix), '{p}LastChange'.format(p=prefix), \
                  '{p}Process'.format(p=prefix), '{p}Property'.format(p=prefix))
    
    for elem in tree.iter():
        if elem.getparent() is not None and elem.tag != 'title' and elem.getparent().tag != 'title':
            elem.getparent().remove(elem)
    
    print("Output_file_structure: "+output_file.replace(".xml", "_structure.xml"))
    tree.write(output_file.replace(".xml", "_structure.xml"))

def create_tree_files(doc_candidates, output_file, tree, prefix):

    title_elems = []
    title_types = {}

    for c in doc_candidates:
        title_elems.append(c[2])  
        title_types[c[2]] = c[1]  

    levels, max_level = get_levels(title_elems, title_types)

    for elem in levels.keys():
        elem.set('title', str(levels[elem]))

    et.strip_tags(tree,'{p}Page'.format(p=prefix))        

    create_xml(output_file, tree, max_level, prefix)

def process_docs():
    
    output_path = "pdfs/processed/tesseract_titled"
    xml_path = "pdfs/processed/tesseract"
    files = os.listdir(xml_path)
    
    prefix = "{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}"
    
    for x in files:

        try: 
            tree = et.parse(os.path.join(xml_path, x)) 
        except:        
            continue
            
        doc_candidates = extract_candidates(tree, prefix)

        create_tree_files(doc_candidates, x, tree, prefix)

def process_doc(input_file,output_file):
	prefix = "{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}"
	try:
		tree = et.parse(input_file) 
	except:
		print("Exception prsing tree")
	doc_candidates = extract_candidates(tree, prefix)
	create_tree_files(doc_candidates, output_file, tree, prefix)

if __name__ == "__main__":
	args = sys.argv[1:]
	input_file = args[0]
	output_file = args[1]
	process_doc(input_file,output_file)
